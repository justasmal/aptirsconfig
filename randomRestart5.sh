#!/bin/bash

sleep 300
while true
do
  array[0]="dbmain"
  array[1]="dbsec"
  array[2]="dbthird"
  cmdToExec="docker-compose -f docker-compose_3api_cockroach.yml restart "
  size=${#array[@]}
  index=$(($RANDOM % $size))
  docmd="$cmdToExec${array[$index]}"
  echo $docmd
  eval $docmd
  sleep 300
done
