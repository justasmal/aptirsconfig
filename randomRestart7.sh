#!/bin/bash

sleep 100
while true
do
  array[0]="pd0"
  array[1]="pd1"
  array[2]="pd2"
  array[3]="tikv0"
  array[4]="tikv1"
  array[5]="tikv2"
  array[6]="tidb"
  cmdToExec="docker-compose -f docker-compose_3api_tidb.yml restart "
  size=${#array[@]}
  index=$(($RANDOM % $size))
  docmd="$cmdToExec${array[$index]}"
  echo $docmd
  eval $docmd
  sleep 300
done
